﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace RedSerenity.Johnny5.Core {
	public static class StartupExtensions {
		public static IServiceCollection AddJohnny5(this IServiceCollection services) {

			return services;
		}

		public static IApplicationBuilder UseJohnny5(this IApplicationBuilder app) {

			return app;
		}
	}
}