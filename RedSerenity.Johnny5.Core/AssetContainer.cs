using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using RedSerenity.Johnny5.Exceptions;
using RedSerenity.Johnny5.FileProviders;
using RedSerenity.Johnny5.FileProviders.Abstractions;

namespace RedSerenity.Johnny5.Core {
	public class AssetContainer : IAssetContainer {
		public Guid ContainerId { get; }
		private readonly IWriteFileProvider _fileProvider;

		public AssetContainer(Guid containerId, IWriteFileProvider fileProvider) {
			ContainerId = containerId;
			_fileProvider = fileProvider;
		}

		public IFileInfo GetFileInfo(string subPath) {
			subPath = ContainerPath(subPath);
			return _fileProvider.GetFileInfo(subPath);
		}

		public IDirectoryContents GetDirectoryContents(string subPath) {
			subPath = ContainerPath(subPath);
			return _fileProvider.GetDirectoryContents(subPath);
		}

		public DirectoryInfo CreateDirectory(string path) {
			path = ContainerPath(path);
			return _fileProvider.CreateDirectory(path);
		}

		public DirectoryInfo RenameDirectory(string existingPath, string newPath) {
			existingPath = ContainerPath(existingPath);
			newPath = ContainerPath(newPath);
			return _fileProvider.RenameDirectory(existingPath, newPath);
		}

		public void DeleteDirectory(string path) {
			path = ContainerPath(path);
			_fileProvider.DeleteDirectory(path);
		}

		public Task<IFileInfo> WriteFile(string path, Stream contents, FileMode fileMode = FileMode.Create) {
			path = ContainerPath(path);
			return _fileProvider.WriteFile(path, contents, fileMode);
		}

		public IFileInfo RenameFile(string file, string newName) {
			file = ContainerPath(file);
			return _fileProvider.RenameFile(file, newName);
		}

		public IFileInfo MoveFile(string file, string path) {
			file = ContainerPath(file);
			path = ContainerPath(path);
			return _fileProvider.MoveFile(file, path);
		}

		public void DeleteFile(string file) {
			_fileProvider.DeleteFile(file);
		}

		private string ContainerPath(string path) {
			if (Path.IsPathRooted(path)) {
				throw new InvalidFileSystemPathException($"Path '{path}' must be relative, not absolute.");
			}

			path = PathUtils.TrimPathStart(path);
			return $"{ContainerId}/{path}";
		}

	}
}