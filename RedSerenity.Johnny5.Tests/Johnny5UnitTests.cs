using System;
using System.IO;
using System.Text;
using NUnit.Framework;
using RedSerenity.Johnny5.Core;
using RedSerenity.Johnny5.FileProviders;

namespace RedSerenity.Johnny5.Tests {
	public class Tests {
		private const string TestDirectory = "/tmp/Johnny5Tests";
		private Guid ContainerId { get; set; }
		private AssetContainer AC { get; set; }
		private string ContainerPath { get; set; }

		[OneTimeSetUp]
		public void Setup() {
			ContainerId = Guid.NewGuid();

			if (Directory.Exists(TestDirectory)) {
				Directory.Delete(TestDirectory, true);
			}

			Directory.CreateDirectory(TestDirectory);

			AC = new AssetContainer(
				ContainerId,
				new PhysicalWriteFileProvider(TestDirectory)
			);

			ContainerPath = $"{TestDirectory}/{ContainerId}";
		}

		[OneTimeTearDown]
		public void Teardown() {
			if (Directory.Exists(TestDirectory)) {
				Directory.Delete(TestDirectory, true);
			}
		}

		[Test]
		public void Test_CreateDirectory() {
			string dirName = Path.GetRandomFileName();
			AC.CreateDirectory(dirName);

			Assert.IsTrue(Directory.Exists($"{ContainerPath}/{dirName}"));
		}

		[Test]
		public void Test_RenameDirectory() {
			string dirName = Path.GetRandomFileName();
			string newName = Path.GetRandomFileName();

			AC.CreateDirectory(dirName);
			AC.RenameDirectory(dirName, newName);

			Assert.IsFalse(Directory.Exists($"{ContainerPath}/{dirName}"));
			Assert.IsTrue(Directory.Exists($"{ContainerPath}/{newName}"));
		}

		[Test]
		public void Test_DeleteDirectory() {
			string dirName = Path.GetRandomFileName();
			AC.CreateDirectory(dirName);
			AC.DeleteDirectory(dirName);

			Assert.IsFalse(Directory.Exists($"{ContainerPath}/{dirName}"));
		}

		private Stream StringToStream(string data) {
			return new MemoryStream(Encoding.UTF8.GetBytes(data));
		}

		private string StreamToString(StreamReader stream) {
			string contents = "";
			using (stream) {
				string s = "";
				while ((s = stream.ReadLine()) != null) {
					contents += s;
				}
			}

			return contents;
		}

		[Test]
		public void Test_CreateNewFile() {
			string fileName = Path.GetRandomFileName();
			string fileContents = "Hello, World!";

			AC.WriteFile(fileName, StringToStream(fileContents), FileMode.Create);

			FileInfo file = new FileInfo($"{ContainerPath}/{fileName}");
			string contents = StreamToString(file.OpenText());

			Assert.IsTrue(file.Exists);
			Assert.AreEqual(fileContents, contents);
		}

		[Test]
		public void Test_WriteToExistingFile() {
			string fileName = Path.GetRandomFileName();
			string fileContents = "Hello, World!";
			string newFileContents = "Hello, Universe!";

			AC.WriteFile(fileName, StringToStream(fileContents), FileMode.Create);

			FileInfo file;
			string contents;

			file = new FileInfo($"{ContainerPath}/{fileName}");
			contents = StreamToString(file.OpenText());

			Assert.IsTrue(file.Exists);
			Assert.AreEqual(fileContents, contents);

			AC.WriteFile(fileName, StringToStream(newFileContents), FileMode.Open);

			file = new FileInfo($"{ContainerPath}/{fileName}");
			contents = StreamToString(file.OpenText());

			Assert.IsTrue(file.Exists);
			Assert.AreEqual(newFileContents, contents);
		}

		[Test]
		public void Test_RenameFile() { }

		[Test]
		public void Test_MoveFile() { }

		[Test]
		public void Test_DeleteFile() { }
	}
}