﻿using System;
using System.Runtime.Serialization;

namespace RedSerenity.Johnny5.Exceptions {
	public class ReadOnlyFileProviderException : Exception {
		public ReadOnlyFileProviderException() { }
		protected ReadOnlyFileProviderException(SerializationInfo info, StreamingContext context) : base(info, context) { }
		public ReadOnlyFileProviderException(string message) : base(message) { }
		public ReadOnlyFileProviderException(string message, Exception innerException) : base(message, innerException) { }
	}
}