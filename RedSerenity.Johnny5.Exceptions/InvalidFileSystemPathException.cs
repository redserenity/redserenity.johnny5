using System;
using System.Runtime.Serialization;

namespace RedSerenity.Johnny5.Exceptions {
	public class InvalidFileSystemPathException : Exception {
		public InvalidFileSystemPathException() { }
		protected InvalidFileSystemPathException(SerializationInfo info, StreamingContext context) : base(info, context) { }
		public InvalidFileSystemPathException(string message) : base(message) { }
		public InvalidFileSystemPathException(string message, Exception innerException) : base(message, innerException) { }
	}
}