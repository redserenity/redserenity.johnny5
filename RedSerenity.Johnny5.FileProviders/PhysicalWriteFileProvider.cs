using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Physical;
using RedSerenity.Johnny5.Exceptions;
using RedSerenity.Johnny5.FileProviders.Abstractions;

namespace RedSerenity.Johnny5.FileProviders {
	public class PhysicalWriteFileProvider : PhysicalFileProvider, IWriteFileProvider {


		public PhysicalWriteFileProvider(string root) : base(root) { }
		public PhysicalWriteFileProvider(string root, ExclusionFilters filters) : base(root, filters) { }

		public bool IsWritable => true;

		#region Directory Methods
		public DirectoryInfo CreateDirectory(string subPath) {
			string fullPath = GetFullPath(subPath);
			return Directory.CreateDirectory(fullPath);
		}

		public DirectoryInfo RenameDirectory(string existingPath, string newPath) {
			string fullExistingPath = GetFullPath(existingPath);
			string fullNewPath = GetFullPath(newPath);

			Directory.Move(fullExistingPath, fullNewPath);
			return new DirectoryInfo(fullNewPath);
		}

		public void DeleteDirectory(string path) {
			string fullPath = GetFullPath(path);
			Directory.Delete(fullPath, true);
		}
		#endregion

		#region File Methods
		public async Task<IFileInfo> WriteFile(string path, Stream contents, FileMode fileMode = FileMode.Create) {
			string fullPath = GetFullPath(path);

			using (FileStream file = new FileStream(fullPath, fileMode, FileAccess.Write, FileShare.None, 3145728, true)) {
				await contents.CopyToAsync(file);
				return new PhysicalFileInfo(new FileInfo(fullPath));
			}
		}

		public IFileInfo RenameFile(string file, string newName) {
			if (!File.Exists(file)) {
				throw new FileNotFoundException(nameof(file));
			}

			File.Move(file, newName);
			return new PhysicalFileInfo(new FileInfo(newName));
		}

		public IFileInfo MoveFile(string file, string path) {
			if (!File.Exists(file)) {
				throw new FileNotFoundException(nameof(file));
			}

			File.Move(file, path);
			return new PhysicalFileInfo(new FileInfo(path));
		}

		public void DeleteFile(string file) {
			if (!File.Exists(file)) {
				throw new FileNotFoundException(nameof(file));
			}

			File.Delete(file);
		}
		#endregion

		#region Helper Functions
		private string GetFullPath(string path) {
			if (String.IsNullOrEmpty(path)) {
				throw new ArgumentNullException(nameof(path));
			}

			if (PathUtils.HasInvalidPathChars(path)) {
				throw new InvalidFileSystemPathException($"Path '{path}' contains invalid characters");
			}

			path = PathUtils.TrimPathStart(path);

			if (Path.IsPathRooted(path)) {
				throw new InvalidFileSystemPathException($"Path '{path}' must be relative, not absolute.");
			}

			if (PathUtils.PathNavigatesAboveRoot(path)) {
				throw new InvalidFileSystemPathException($"Path ''{path}'' is invalid.");
			}

			string fullPath;
			try {
				fullPath = Path.GetFullPath(Path.Combine(Root, path));
			} catch {
				throw new InvalidFileSystemPathException($"Path ''{path}'' is invalid.");
			}

			if (!IsUnderneathRoot(fullPath)) {
				throw new InvalidFileSystemPathException($"Path ''{path}'' is invalid.");
			}

			return fullPath;
		}

		private bool IsUnderneathRoot(string fullPath) {
			return fullPath.StartsWith(Root, StringComparison.OrdinalIgnoreCase);
		}
		#endregion
	}
}