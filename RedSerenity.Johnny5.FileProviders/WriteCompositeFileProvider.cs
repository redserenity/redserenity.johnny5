using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using RedSerenity.Johnny5.Exceptions;
using RedSerenity.Johnny5.FileProviders.Abstractions;

namespace RedSerenity.Johnny5.FileProviders {
	public class WriteCompositeFileProvider : IWriteFileProvider {
		private IList<IWriteFileProvider> _fileProviders { get; }

		/// <summary>
		/// Initializes a new instance of the <see cref="WriteCompositeFileProvider" /> class using a collection of file providers.
		/// </summary>
		/// <param name="fileProviders">The collection of <see cref="IWriteFileProvider" /></param>
		public WriteCompositeFileProvider(params IWriteFileProvider[] fileProviders) {
			_fileProviders = fileProviders ?? new IWriteFileProvider[0];
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WriteCompositeFileProvider" /> class using a collection of file providers.
		/// </summary>
		/// <param name="fileProviders">The collection of <see cref="IWriteFileProvider" /></param>
		public WriteCompositeFileProvider(IEnumerable<IWriteFileProvider> fileProviders) {
			if (fileProviders == null) {
				throw new ArgumentNullException(nameof(fileProviders));
			}

			_fileProviders = fileProviders.ToList();
		}

		public bool IsWritable {
			get { return _fileProviders.Any(fileProvider => fileProvider.IsWritable); }
		}

		DirectoryInfo IWriteFileProvider.CreateDirectory(string path) {
			if (!IsWritable) {
				throw new ReadOnlyFileProviderException(nameof(path));
			}

			foreach (var fileProvider in _fileProviders) {
				// Grab the first writable file provider. Order matters in registration!
				if (fileProvider.IsWritable) {
					return fileProvider.CreateDirectory(path);
				}
			}

			return null;
		}

		public DirectoryInfo RenameDirectory(string existingPath, string newPath) {
			if (!IsWritable) {
				throw new ReadOnlyFileProviderException(nameof(newPath));
			}

			foreach (var fileProvider in _fileProviders) {
				// Grab the first writable file provider. Order matters in registration!
				if (fileProvider.IsWritable) {
					return fileProvider.RenameDirectory(existingPath, newPath);
				}
			}

			return null;
		}

		public void DeleteDirectory(string path) {
			if (!IsWritable) {
				throw new ReadOnlyFileProviderException(nameof(path));
			}

			foreach (var fileProvider in _fileProviders) {
				// Grab the first writable file provider. Order matters in registration!
				if (fileProvider.IsWritable) {
					fileProvider.DeleteDirectory(path);
					return;
				}
			}
		}

		public async Task<IFileInfo> WriteFile(string path, Stream contents, FileMode fileMode = FileMode.Create) {
			if (!IsWritable) {
				throw new ReadOnlyFileProviderException(nameof(path));
			}

			foreach (var fileProvider in _fileProviders) {
				// Grab the first writable file provider. Order matters in registration!
				if (fileProvider.IsWritable) {
					return await fileProvider.WriteFile(path, contents, fileMode);
				}
			}

			return null;
		}

		public IFileInfo RenameFile(string file, string newName) {
			if (!IsWritable) {
				throw new ReadOnlyFileProviderException(nameof(file));
			}

			foreach (var fileProvider in _fileProviders) {
				// Grab the first writable file provider. Order matters in registration!
				if (fileProvider.IsWritable) {
					return fileProvider.RenameFile(file, newName);
				}
			}

			return null;
		}

		public IFileInfo MoveFile(string file, string path) {
			if (!IsWritable) {
				throw new ReadOnlyFileProviderException(nameof(file));
			}

			foreach (var fileProvider in _fileProviders) {
				// Grab the first writable file provider. Order matters in registration!
				if (fileProvider.IsWritable) {
					return fileProvider.MoveFile(file, path);
				}
			}

			return null;
		}

		public void DeleteFile(string file) {
			if (!IsWritable) {
				throw new ReadOnlyFileProviderException(nameof(file));
			}

			foreach (var fileProvider in _fileProviders) {
				// Grab the first writable file provider. Order matters in registration!
				if (fileProvider.IsWritable) {
					fileProvider.DeleteFile(file);
					return;
				}
			}

			return;
		}

		public IEnumerable<IWriteFileProvider> FileProviders => _fileProviders;

		public IFileInfo GetFileInfo(string subPath) {
			foreach (var fileProvider in _fileProviders) {
				var fileInfo = fileProvider.GetFileInfo(subPath);
				if (fileInfo != null && fileInfo.Exists) {
					return fileInfo;
				}
			}

			return new NotFoundFileInfo(subPath);
		}

		public IDirectoryContents GetDirectoryContents(string subPath) {
			return new WriteCompositeDirectoryContents(_fileProviders, subPath);
		}

		/// <summary>
		/// Creates a <see cref="IChangeToken"/> for the specified <paramref name="pattern"/>.
		/// </summary>
		/// <param name="filter">Filter string used to determine what files or folders to monitor. Example: **/*.cs, *.*, subFolder/**/*.cshtml.</param>
		/// <returns>An <see cref="IChangeToken"/> that is notified when a file matching <paramref name="pattern"/> is added, modified or deleted.
		/// The change token will be notified when one of the change token returned by the provided <see cref="IWriteFileProvider"/> will be notified.</returns>
		public IChangeToken Watch(string filter) {
			// Watch all file providers
			var changeTokens = new List<IChangeToken>();
			foreach (var fileProvider in _fileProviders) {
				var changeToken = fileProvider.Watch(filter);
				if (changeToken != null) {
					changeTokens.Add(changeToken);
				}
			}

			// There is no change token with active change callbacks
			if (changeTokens.Count == 0) {
				return NullChangeToken.Singleton;
			}

			return new CompositeChangeToken(changeTokens);
		}
	}
}