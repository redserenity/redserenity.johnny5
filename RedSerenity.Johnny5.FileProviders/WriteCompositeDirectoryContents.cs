using System.Collections.Generic;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Composite;
using RedSerenity.Johnny5.FileProviders.Abstractions;

namespace RedSerenity.Johnny5.FileProviders {
	public class WriteCompositeDirectoryContents : CompositeDirectoryContents {
		public WriteCompositeDirectoryContents(IList<IWriteFileProvider> fileProviders, string subPath) : base(fileProviders as IList<IFileProvider>, subPath) { }
	}
}