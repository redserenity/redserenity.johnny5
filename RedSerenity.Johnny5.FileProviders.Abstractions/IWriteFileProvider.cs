﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;

namespace RedSerenity.Johnny5.FileProviders.Abstractions {
	/// <summary>
	/// A read/write file provider abstraction.
	/// </summary>
	public interface IWriteFileProvider : IFileProvider {
		bool IsWritable { get; }

		/// <summary>
		/// Create a directory at the given path, including any parent paths.
		/// </summary>
		/// <param name="path">New directory path to create.</param>
		/// <returns>Void</returns>
		DirectoryInfo CreateDirectory(string path);
		DirectoryInfo RenameDirectory(string existingPath, string newPath);
		void DeleteDirectory(string path);

		Task<IFileInfo> WriteFile(string path, Stream contents, FileMode fileMode = FileMode.Create);

		/// <summary>
		/// Rename a file at the given path.
		/// </summary>
		/// <param name="file">IFileInfo of specific file to rename.</param>
		/// <param name="newName">The new file name, including extension.</param>
		/// <returns>The file information.</returns>
		IFileInfo RenameFile(string file, string newName);

		/// <summary>
		/// Moves a file from one path to another.
		/// </summary>
		/// <param name="file">IFileInfo of specific file to move.</param>
		/// <param name="path">The path to move the file to.</param>
		/// <returns>The file information.</returns>
		IFileInfo MoveFile(string file, string path);

		/// <summary>
		/// Delete a file at the given path.
		/// </summary>
		/// <param name="file">IFileInfo of specific file to delete.</param>
		/// <returns>Void</returns>
		void DeleteFile(string file);
	}
}