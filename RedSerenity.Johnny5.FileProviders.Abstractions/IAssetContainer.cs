using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;

namespace RedSerenity.Johnny5.FileProviders.Abstractions {
	public interface IAssetContainer {
		Guid ContainerId { get; }

		IFileInfo GetFileInfo(string subPath);
		IDirectoryContents GetDirectoryContents(string subPath);

		DirectoryInfo CreateDirectory(string path);
		DirectoryInfo RenameDirectory(string existingPath, string newPath);
		void DeleteDirectory(string path);

		Task<IFileInfo> WriteFile(string path, Stream contents, FileMode fileMode = FileMode.Create);
		IFileInfo RenameFile(string file, string newName);
		IFileInfo MoveFile(string file, string path);
		void DeleteFile(string file);
	}
}